import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sing-in',
  templateUrl: './sing-in.component.html',
  styleUrls: ['./sing-in.component.scss']
})
export class SingInComponent implements OnInit {
  logInForm: FormGroup;
  submitted = false;
  constructor(private fb: FormBuilder, private router:Router) {
    this.logInForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }
  ngOnInit(): void {
  }
  onSubmit(){
    this.submitted = true;
    console.log(this.logInForm.value);
    if(this.logInForm.valid){
      this.router.navigate(['/user']);
    }
  }
}
