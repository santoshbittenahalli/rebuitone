import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RAutoCompleteComponent } from './r-auto-complete.component';

describe('RAutoCompleteComponent', () => {
  let component: RAutoCompleteComponent;
  let fixture: ComponentFixture<RAutoCompleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RAutoCompleteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RAutoCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
