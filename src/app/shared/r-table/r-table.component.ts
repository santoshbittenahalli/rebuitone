import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-r-table',
  templateUrl: './r-table.component.html',
  styleUrls: ['./r-table.component.scss']
})
export class RTableComponent implements OnInit {
  @Input() dataSource: any;
  @Input() displayedColumns: any;
  constructor() { }

  ngOnInit(): void { }

}
