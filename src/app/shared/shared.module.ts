import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RTableComponent } from './r-table/r-table.component';

import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import {CdkTableModule} from '@angular/cdk/table';
import { RAutoCompleteComponent } from './r-auto-complete/r-auto-complete.component';

@NgModule({
  declarations: [
    RTableComponent,
    RAutoCompleteComponent
  ],
  imports: [
    CommonModule,
    CdkTableModule,
    MatTableModule,
    MatIconModule
  ],
  exports:[
    RTableComponent
  ]
})
export class SharedModule { }
