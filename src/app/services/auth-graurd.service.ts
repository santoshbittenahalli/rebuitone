import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGraurdService {
    constructor(private router:Router, private authService: AuthService) { }
  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean|UrlTree {

if (!this.authService.isUserLoggedIn()) {
this.router.navigate(["login"],{ queryParams: { retUrl: route.url} });
return false;

//var urlTree = this.router.createUrlTree(['login']);
//return urlTree;
}

return true;
}
}
