import { TestBed } from '@angular/core/testing';

import { AuthGraurdService } from './auth-graurd.service';

describe('AuthGraurdService', () => {
  let service: AuthGraurdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthGraurdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
