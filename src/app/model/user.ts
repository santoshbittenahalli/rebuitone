import { Address } from './address'
export interface User {
  name: string;
  address: string;
  role: string;
  email: string;
  phone: string;
}
