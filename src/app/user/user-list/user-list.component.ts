import { Component, OnInit } from '@angular/core';
import { User } from '../../model/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {


  }
  ELEMENT_DATA: User[] = [
    { name: 'lulu22', address:'#233, address111, 57142', role: 'admin1', email: 'san-test1@test.com', phone: '9489213421'},
    { name: 'shock1', address:'#233, address111, 57141', role: 'admin2', email: 'san-test2@test.com', phone: '9489213425'},
    { name: 'shock2', address:'#233, address111, 57145', role: 'admin3', email: 'san-test3@test.com', phone: '9489213426'},
    { name: 'shock3', address:'#233, address111, 57146', role: 'admin4', email: 'san-test4@test.com', phone: '9489213427'},
    { name: 'shock4', address:'#233, address111, 57147', role: 'admin5', email: 'san-test5@test.com', phone: '9489213421'}
  ];

  columns = [
    'name',
    'address',
    'role',
    'email',
    'phone'
  ];

}
